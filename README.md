# Usage

## Create working dir with repository:

	git clone https://gitlab.com/daoud.sie/covid-19-monitor.git ./my_work_dir
	cd my_work_dir

## Specify human / covid reference and gff files in config/config.yaml

	nano config/config.yaml

## Specify location of fasta files in snakemake command:

	snakemake -j16 --use-conda --rerun-incomplete --config inputPath=[path to fastq]

## Snakemake command with slurm support:

	snakemake --cluster "sbatch -c {threads} -o logs/slurm/%j.out" -j999 --use-conda --rerun-incomplete --config inputPath=[path to fastq]
